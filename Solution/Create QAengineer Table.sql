USE [BugTracking]
GO

/****** Object:  Table [dbo].[QAengineer]    Script Date: 08/24/2012 12:56:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[QAengineer](
	[EmployeeID] [int] NOT NULL,
	[NumTestsWritten] [int] NULL,
 CONSTRAINT [PK_QAengineer] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[QAengineer]  WITH CHECK ADD  CONSTRAINT [FK_QAengineer_Employee] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeID])
GO

ALTER TABLE [dbo].[QAengineer] CHECK CONSTRAINT [FK_QAengineer_Employee]
GO

