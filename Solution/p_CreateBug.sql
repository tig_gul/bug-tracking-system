USE [BugTracking]
GO

/****** Object:  StoredProcedure [dbo].[p_CreateBug]    Script Date: 08/24/2012 13:12:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[p_CreateBug]
	@QAengineerID int,
	@Desc nvarchar(255),
	@Severity int,	
	@Priority int	
AS
BEGIN
	INSERT INTO Bugs
		([Description], Severity, Priority, Complete, DateEntered, EmployeeID)
	VALUES
		(@Desc, @Severity, @Priority, '0', GetDate(), @QAengineerID)
	
	DECLARE @ID INT
	
	SET @ID = (SELECT Max(BugID)FROM Bugs)	
	
	PRINT 'Bug created successfully.' + char(13)
	PRINT 'Bug ID: ' + CAST(@ID AS VARCHAR(5)) + ' - ' + @Desc + char(13)
	PRINT 'Severity: ' + CAST(@Severity AS VARCHAR(5)) + char(13)
	PRINT 'Priority: ' + CAST(@Priority AS VARCHAR(5))
	
	Return 1
END

GO

