USE [BugTracking]
GO

/****** Object:  Table [dbo].[SoftwareDeveloper]    Script Date: 08/24/2012 12:56:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SoftwareDeveloper](
	[EmployeeID] [int] NOT NULL,
	[ProgrammingLanguage] [varchar](50) NULL,
	[HoursDeveloping] [int] NULL,
 CONSTRAINT [PK_SoftDeveloper] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[SoftwareDeveloper]  WITH CHECK ADD  CONSTRAINT [FK_SoftwareDeveloper_Employee] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employee] ([EmployeeID])
GO

ALTER TABLE [dbo].[SoftwareDeveloper] CHECK CONSTRAINT [FK_SoftwareDeveloper_Employee]
GO

