USE [BugTracking]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_SearchBugs]    Script Date: 08/24/2012 12:59:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_SearchBugs]
(	
	@Desc nvarchar(50)
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT Bugs.BugID, Bugs.[Description] AS Bug_Description, Bugs.Severity AS Bug_Severity, Bugs.Priority AS Bug_Priority, Bugs.Complete As Bug_isFixed, Bugs.DateEntered AS DateBugEntered, Fixes.FixID AS FixID, Fixes.[Description] AS Fix_Description, Employee.FirstName + ' ' + Employee.LastName AS QAengineer
	FROM Bugs
		LEFT JOIN lu_Fix_Bug ON Bugs.BugID = lu_Fix_Bug.BugID
			LEFT JOIN Fixes ON lu_Fix_Bug.FixID=Fixes.FixID
				INNER JOIN Employee ON Bugs.EmployeeID=Employee.EmployeeID
	WHERE Bugs.[Description] like '%' + @Desc + '%'
)

GO

