USE [BugTracking]
GO

/****** Object:  StoredProcedure [dbo].[p_CreateFix]    Script Date: 08/24/2012 13:12:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[p_CreateFix]
	@SoftwareDeveloperID int,
	@Desc nvarchar(255),
	@BugsList varchar(255) 
AS
BEGIN

	DECLARE @StrBugID VARCHAR(10)
	DECLARE @Pos INT
	DECLARE @NextPos INT
	DECLARE @StrBugsList VARCHAR(255)

	DECLARE @FixID INT;

	BEGIN TRY
		BEGIN TRANSACTION
			
			INSERT INTO Fixes
				([Description], DateEntered, EmployeeID)
			VALUES
				(@Desc, GetDate(), @SoftwareDeveloperID)
			
			--Get the FixID for the fix that was just inserted
			SET @FixID = (SELECT MAX(FixID) FROM Fixes) 
			
			SET @StrBugsList = @BugsList + ','
			SET @Pos = charindex(',',@StrBugsList)

			WHILE (@pos <> 0)
				BEGIN
				SET @StrBugID = substring(@StrBugsList,1,@Pos - 1)
				
				INSERT INTO lu_Fix_Bug(FixID, BugID) VALUES(@FixID, CAST(@StrBugID AS INT))
				
				UPDATE Bugs SET Complete=1 WHERE BugID=CAST(@StrBugID AS INT)

				SET @StrBugsList = substring(@StrBugsList,@pos+1,len(@StrBugsList))
				SET @pos = charindex(',', @StrBugsList)
			END 
		
		COMMIT TRANSACTION
		
		PRINT 'Fix created successfully.' + char(13)
		PRINT 'Fix ID: ' + CAST(@FixID AS VARCHAR(5)) + ' - ' + @Desc + char(13)
		PRINT 'This fix applies to bug ID(s): ' + @BugsList
		
		Return 1
		
	END TRY
	
	BEGIN CATCH
        ROLLBACK TRANSACTION
		
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
		
	END CATCH
END 


GO

