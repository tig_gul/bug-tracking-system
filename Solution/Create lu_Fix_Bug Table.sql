USE [BugTracking]
GO

/****** Object:  Table [dbo].[lu_Fix_Bug]    Script Date: 08/24/2012 12:56:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[lu_Fix_Bug](
	[FixID] [int] NOT NULL,
	[BugID] [int] NOT NULL,
 CONSTRAINT [PK_lu_Fix_Bug] PRIMARY KEY CLUSTERED 
(
	[FixID] ASC,
	[BugID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[lu_Fix_Bug]  WITH CHECK ADD  CONSTRAINT [FK_lu_Fix_Bug_Bugs] FOREIGN KEY([BugID])
REFERENCES [dbo].[Bugs] ([BugID])
GO

ALTER TABLE [dbo].[lu_Fix_Bug] CHECK CONSTRAINT [FK_lu_Fix_Bug_Bugs]
GO

ALTER TABLE [dbo].[lu_Fix_Bug]  WITH CHECK ADD  CONSTRAINT [FK_lu_Fix_Bug_Fixes] FOREIGN KEY([FixID])
REFERENCES [dbo].[Fixes] ([FixID])
GO

ALTER TABLE [dbo].[lu_Fix_Bug] CHECK CONSTRAINT [FK_lu_Fix_Bug_Fixes]
GO

